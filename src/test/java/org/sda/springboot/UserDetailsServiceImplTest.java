package org.sda.springboot;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.sda.springboot.entities.UserEntity;
import org.sda.springboot.repositories.UserRepository;
import org.sda.springboot.service.UserDetailsServiceImpl;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserDetailsServiceImplTest {

    @Mock
    UserRepository userRepository;
    @InjectMocks
    UserDetailsServiceImpl userDetailsServiceImpl;

    @Test
    public void loadUserByUsernameShouldReturnCorrectUserDetails() {
        //Given
        String username = "Oli";
        when(userRepository.findByUsername(username)).thenReturn(new UserEntity(3, username, "pass"));
        //When
        UserDetails userDetails = userDetailsServiceImpl.loadUserByUsername(username);
        //Then
        Assertions.assertEquals("Oli", userDetails.getUsername());
    }
}
