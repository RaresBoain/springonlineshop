package org.sda.springboot.repositories;

import org.sda.springboot.entities.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Integer> {

    //Query methods - findAllByProductNameContaining o metoda definita de noi si creata de spring pentru a crea query de cautare a produsului dupa nume...metoda va fi apelata din controller
    public List<ProductEntity> findAllByProductNameContaining(String productName);

}
