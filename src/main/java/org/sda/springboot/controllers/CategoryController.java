package org.sda.springboot.controllers;

import org.sda.springboot.entities.CategoryEntity;
import org.sda.springboot.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping(path = "/web")
public class CategoryController {

    @Autowired
    private CategoryRepository categoryRepository;

    @GetMapping("/categories/add")
    public ModelAndView addCategory() {
        ModelAndView modelAndView = new ModelAndView("admin/category-form");
        modelAndView.addObject("category", new CategoryEntity());
        return modelAndView;
    }

    @PostMapping("/categories/save")
    public ModelAndView saveCategory(@ModelAttribute(name = "category")
                                        @Valid CategoryEntity category,
                                    BindingResult bindingResult) {//@ModelAttribute ProductEntity salveaza produsul prin butonul submit
                                                                  //@Valid - valideaza dupa anumite reguli(in productEntity am pus 2 validari-campul nu poate fi gol si min 2 + max 7 caractere)
                                                                  //BindingResult - va contine rezultatul validarii
        ModelAndView modelAndView = new ModelAndView("redirect:/web/products");// dupa ce salvam formularul suntem redirectionati catre lista de produse
        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("admin/category-form");
            modelAndView.addObject("category", category);
            return modelAndView;//modelAndView returneaza pagina web + obiectele care se folosesc in HTML
        }
        categoryRepository.save(category);
        //urmatoarea linie se va folosi doar daca in contructorul modelAndView punem fisierul "products"
        //modelAndView.addObject("products",productRepository.findAll());
        return modelAndView;
    }

    @GetMapping("/categories/delete/{id}")
    public ModelAndView deleteCategory(@PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView("redirect:/web/products");
        categoryRepository.deleteById(id);
        return modelAndView;
    }
}
