package org.sda.springboot.controllers;

import org.sda.springboot.entities.ProductEntity;
import org.sda.springboot.repositories.CategoryRepository;
import org.sda.springboot.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(path = "/web")
public class ProductController {
    @Autowired //injectez ProductRepository
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    public ProductController() {
        System.out.println(getClass().getSimpleName() + " created");
    }

    @GetMapping("/products")
    public ModelAndView getProducts(Model model) {
        ModelAndView modelAndView = new ModelAndView("admin/products");
        if (model.asMap().get("searchedProducts") != null){
            modelAndView.addObject("products", model.asMap().get("searchedProducts"));
        }else {
            modelAndView.addObject("products", productRepository.findAll());
        }
        modelAndView.addObject("categories",categoryRepository.findAll());
        return modelAndView;
    }

    @GetMapping("/products/add")
    public ModelAndView addProduct() {
        ModelAndView modelAndView = new ModelAndView("admin/product-form");
        modelAndView.addObject("product", new ProductEntity());
        modelAndView.addObject("categories", categoryRepository.findAll());
        return modelAndView;
    }

    @GetMapping("/products/edit/{id}")
    public ModelAndView editProduct(@PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView("admin/product-form");
        modelAndView.addObject("product", productRepository.findById(id).get());
        modelAndView.addObject("categories",categoryRepository.findAll());
        return modelAndView;
    }

    @PostMapping("/products/save")
    public ModelAndView saveProduct(@ModelAttribute(name = "product") @Valid ProductEntity product, BindingResult bindingResult) {//@ModelAttribute ProductEntity salveaza produsul prin butonul submit
        //@Valid - valideaza dupa anumite reguli(in productEntity am pus 2 validari-campul nu poate fi gol si min 2 + max 7 caractere)
        //BindingResult - va contine rezultatul validarii
        ModelAndView modelAndView = new ModelAndView("redirect:/web/products");// dupa ce salvam formularul suntem redirectionati catre lista de produse
        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("product-form");
            modelAndView.addObject("product", product);
            return modelAndView;//modelAndView returneaza pagina web + obiectele care se folosesc in HTML
        }
        productRepository.save(product);
        //urmatoarea linie se va folosi doar daca in contructorul modelAndView punem fisierul "products"
        //modelAndView.addObject("products",productRepository.findAll());
        return modelAndView;
    }

    @GetMapping("/products/delete/{id}")
    //@PathVariable - parametru care se pune in URL dupa /
    public ModelAndView deleteProduct(@PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView("redirect:/web/products");
        productRepository.deleteById(id);
        return modelAndView;
    }

    @RequestMapping("/products/search")
    //@RequestParam - parametru in URL care se pune sintaxa-->(?"nume parametru"="valoare parametru")
    public ModelAndView searchProduct(@RequestParam ("searchString") String productName, RedirectAttributes redirectAttributes){
        List<ProductEntity> productEntityList = productRepository.findAllByProductNameContaining(productName);
        ModelAndView modelAndView = new ModelAndView("redirect:/web/products");
        modelAndView.addObject("searchedProducts", productEntityList);
        redirectAttributes.addFlashAttribute("searchedProducts", productEntityList);
        return modelAndView;
    }


}
