package org.sda.springboot.controllers;

import org.sda.springboot.entities.ProductEntity;
import org.sda.springboot.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path="/api") // Daca folosim atnotarea @RequestMapping(path="/api") nu mai scriem la atnotarea metodelor inca o data /api ca in ex urmator @GetMapping("/api/products/{id}")
public class ProductRestApiController {

    @Autowired
    private ProductRepository productRepository;

    public ProductRestApiController() {
        System.out.println(getClass().getSimpleName() + " created");
    }

    @GetMapping("/products")
    public List<ProductEntity> getProducts(){
        return productRepository.findAll();
    }

    @PostMapping("/products/save")
    public String saveProduct(@RequestBody ProductEntity productEntity){
        productRepository.save(productEntity);
        return "Succes";
    }

    @GetMapping("/products/{id}") //numele variabilei de pe linia 30 "{id}" trebuie sa fie exact acelasi ca si numele variabilei ca parametru de la metoda "id" linia 31
    public ProductEntity getProductById(@PathVariable(name = "id") Integer productId){// Prin (name = "id") am specificat ca param productId va avea numele de "id" pentru a se identifica cu "id" de pe linia 30 sau putem pune direct "id" in loc de product"id"
        return productRepository.findById(productId).get();
    }

    @GetMapping("/productById")
    public ProductEntity getProductByIdParam(@RequestParam(name = "id", defaultValue = "4") Integer productId){
 // defaultValue = "4" imi va returna id-ul 4 daca nu gaseste un id de afisat
 //       return productRepository.findById(productId).get();
        Optional<ProductEntity> productEntityOptional = productRepository.findById(productId);
        if (productEntityOptional.isPresent()) {

            return productEntityOptional.get();
        }else {
            return new ProductEntity();
        }
    }

    @GetMapping("/productByIdNew")
    // aceasta metoda imi returneaza un text NOT FOUND in cazul in care id-ul cautat nu exista
    public ResponseEntity<ProductEntity> getProductByIdParamNew(@RequestParam(name = "id", defaultValue = "4") Integer productId){
        Optional<ProductEntity> productEntityOptional = productRepository.findById(productId);
        if (productEntityOptional.isPresent()) {
            return ResponseEntity.ok().body(productEntityOptional.get());
        }else {
            return ResponseEntity.status(404).header("X-Error","Not Found!!!").body(null);
        }
    }

}
