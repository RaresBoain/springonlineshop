package org.sda.springboot.config;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

//@SpringBootApplication
//@ComponentScan("org.sda.springboot")
//@EntityScan("org.sda.springboot.entities")// cu aceasta linie scanam si folderul entities...altfel nu ajungem acolo...trebuie sa ii spunem noi explicit pentru ca AppConfig se afla in subpachet si nu inafara ca si App
//// Din acest folder "org.sda.springboot" va incepe scanarea claselor aplicatiei mele si citeste atnotarile scrise
//@EnableJpaRepositories("org.sda.springboot.repositories")
//@Import({WebConfig.class})
//public class AppConfig {
//    @Bean//Se foloseste pt a instantia si ai spune springului sa memoreze instanta unei clase si sa o foloseasca la injectare in alte clase prin @Autowire
//    public Bean3 createBean3() {
//        return new Bean3();
//    }
//}
